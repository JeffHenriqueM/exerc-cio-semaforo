package ab1ab2;
import java.util.concurrent.Semaphore;

public class Teste {
    public static void main(String[] args) {
        StringFinal stringFinal = new StringFinal();
        Semaphore semaforoA = new Semaphore(0);
        Semaphore semaforoB = new Semaphore(0);
        
        Thread thrdA = new Thread(new AccumulatorA(semaforoA, semaforoB, stringFinal));
        Thread thrdB = new Thread(new AccumulatorB(semaforoA, semaforoB, stringFinal));
        
        thrdA.start();
        thrdB.start();
        
        try{
            thrdA.join();
            thrdB.join();
        }catch(InterruptedException ie){
            ie.printStackTrace();
        }
        
        System.out.println(stringFinal.getString());
    }
}
