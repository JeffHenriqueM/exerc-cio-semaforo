package ab1ab2;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

class StringFinal {
    StringBuffer stringFinal;
    
    public StringFinal() {
        stringFinal = new StringBuffer("");
    }

    void inserirCaracter(Character s) {
        stringFinal.append(s);
    }
    
    public String getString(){
        return stringFinal.toString();
    }
    
}


class AccumulatorA implements Runnable{
    private Semaphore semaforoA, semaforoB;
    StringFinal StringFinal;

    public AccumulatorA(Semaphore semaforoA, Semaphore semaforoB, StringFinal stringFinal) {
        this.semaforoA = semaforoA;
        this.semaforoB = semaforoB;
        this.StringFinal = stringFinal;
    }
    
    @Override
    public void run() {
        StringFinal.inserirCaracter('A');
        try{
            semaforoB.release();
            semaforoA.acquire();
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        StringFinal.inserirCaracter('a');
    }
}

class AccumulatorB implements Runnable{
    private Semaphore semaforoA, semaforoB;
    StringFinal stringFinal;

    public AccumulatorB(Semaphore semaforoA, Semaphore semaforoB, StringFinal stringFinal) {
        this.semaforoA = semaforoA;
        this.semaforoB = semaforoB;
        this.stringFinal = stringFinal;
    }
    
    @Override
    public void run() {
        stringFinal.inserirCaracter('B');
        try{
            semaforoA.release();
            semaforoB.acquire();
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        stringFinal.inserirCaracter('b');
    }
}
